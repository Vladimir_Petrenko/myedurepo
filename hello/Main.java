package com.hello;
import com.welcome.Hello;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Hello hello = new Hello();
        Scanner in = new Scanner(System.in);
        System.out.println("Enter name ");

        hello.setupName(in.nextLine());
        hello.welcome();

        System.out.print("Hello world!");
        hello.byebye();
    }
}